import java.io.BufferedReader;
import java.io.FileReader;
import static javax.swing.JFrame.EXIT_ON_CLOSE; 
import java.awt.*; 
import java.io.File;
import java.io.IOException;
import java.util.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

class RunnableDemo2 implements Runnable {
   private Thread t;
   private String threadName;
   
 //Declare and initialize all needed variables
   String [] parts;
   int col = 0;
   String unit = null;
	 String line, line1, workoutDate;
	 boolean isSpeed = true, isPower = true, isCadence = true, isAltitude = true;
	 
	 //create arraylist for all HRData for easy management
//	 ArrayList<ArrayList<Integer>> speed = new ArrayList<ArrayList<Integer>>();
	 static ArrayList<String> dates = new ArrayList<String>();
	 ArrayList<Double> speed = new ArrayList<Double>();
	 ArrayList<Integer> cadence = new ArrayList<Integer>();
	 ArrayList<Integer> altitude = new ArrayList<Integer>();
	 ArrayList<Integer> heartRate = new ArrayList<Integer>();
	 ArrayList<Integer> power = new ArrayList<Integer>();
	 Map<String, String> params = new HashMap<String, String>();
	 Map<String, String> units = new HashMap<String, String>();
	 Map<String, Double> maxSpeedMap = new HashMap<String, Double>();
	 Map<String, Double> averageSpeedMap = new HashMap<String, Double>();
	 Map<String, Double> maxPowerMap = new HashMap<String, Double>();
	 Map<String, Double> averagePowerMap = new HashMap<String, Double>();
	 Map<String, Double> maxAltitudeMap = new HashMap<String, Double>();
	 Map<String, Double> averageAltitudeMap = new HashMap<String, Double>();
	 Map<String, Double> maxHeartRateMap = new HashMap<String, Double>();
	 Map<String, Double> minHeartRateMap = new HashMap<String, Double>();
	 Map<String, Double> averageHeartRateMap = new HashMap<String, Double>();
   
   RunnableDemo2( String name) {
      threadName = name;
      System.out.println("Creating Thread for file: " +  threadName );
   }
   
   RunnableDemo2() {
	   
   }
   
   public void run() {
      System.out.println("Running Thread for file: " +  threadName );
      try {
    	//create a buffered reader to read from and write to file
    		 BufferedReader br = new BufferedReader(new FileReader("F:/Year 3/Sem C/ASEB/ASDBdata-20160805T031015Z/ASDBdata/August2012DataForCalandar/"+threadName));
    		// BufferedWriter out1 = new BufferedWriter(new FileWriter("output1.txt"));
    		// BufferedWriter out = new BufferedWriter(new FileWriter("output.txt"));
    		 
    		 //begin reading from the file and capture data after the word "Monitor" is found
    		 while ((line = br.readLine()) != null) {
    		  if (line.contains("Monitor")){
    		    // do something
    		    break; // breaks the while loop
    		  }
    		 }
    		 
    		 // we reached the section we want
    		 while ((line = br.readLine()) != null) {
    		    
    			 // read everything before the word "Interval"
    			 if (line.contains("Interval")){
    				    // do something
    				    break; // breaks the while loop
    				  }
    			 
    			 //split the [Params] into their components and put them in the Map variable params
    			 parts = line.split("\\=");
    			
    			params.put(parts[0], parts[1]);
    			
    			//System.out.println(parts[0] +" - "+ params.get(parts[0]));
    			
    			col +=1;
    		 }
    		 
    		 String[] dateFromFile = params.get("Date").split("(?<=.)");
    		 workoutDate = dateFromFile[6]+dateFromFile[7]+"/"+dateFromFile[4]+dateFromFile[5]+"/"+dateFromFile[0]+dateFromFile[1]+dateFromFile[2]+dateFromFile[3];
    		 dates.add(workoutDate);
    		 
    		 // Get the last last bit of te SMode to determine the units of the measurements
    		 //char[] digits = params.get("SMode").toCharArray();
    		 
    		 String[] digits = params.get("SMode").split("(?<=.)");
    		 
    		// System.out.print(digits[digits.length-1]);
    		 
    		 if(digits[0].equalsIgnoreCase("0")){
    			 isSpeed = false;
    		 }
    		 
    		 if(digits[1].equalsIgnoreCase("0")){
    			 isCadence = false;
    		 }
    		 
    		 if(digits[2].equalsIgnoreCase("0")){
    			 isAltitude = false;
    		 }
    		 
    		 if(digits[3].equalsIgnoreCase("0")){
    			 isPower = false;
    		 }
    		 
    		 if(digits.length == 9) {
    			unit = digits[(digits.length -1)];
    			 
    			 //System.out.print("Smode is 9, so Unit is: "+unit);
    		 } else if(digits.length == 8) {
    			 unit = digits[(digits.length)];
    		 }
    		 
    		 //if the bit = 0, then the unit is metric
    		 if(unit.equalsIgnoreCase("0")) {
    			 units.put("speed", "km/h");
    			 units.put("dist", "km");
    			 units.put("altitude", "m");
    			 units.put("temp", "oC");
    			 units.put("power", "watts");
    			 units.put("cadence", "rpm");
    			 units.put("heartRate", "bpm");
    			 
    			 /** For Debugging purposes
    			 System.out.print("Unit is 0");
    			 System.out.print("Speed is: " + units.get("speed"));
    			 */
    		 } 
    		 // if the bit is 1, then the unit is imperial
    		 else if(unit.equalsIgnoreCase("1")) {
    			 units.put("speed", "mph");
    			 units.put("dist", "m");
    			 units.put("altitude", "ft");
    			 units.put("temp", "oF");
    			 units.put("power", "watts");
    			 units.put("cadence", "rpm");
    			 units.put("heartRate", "bpm");
    			 
    			 System.out.print("Unit is 1");
    		 }
    		 
    		 //proceed to reda in the [HRData]
    		 while ((line1 = br.readLine()) != null) {
    			  if (line1.contains("[HRData]")){
    			    // do something
    			    break; // breaks the while loop
    			  }
    			 }
    				
    				//print the HRData out in JTable
    				DefaultTableModel model = new DefaultTableModel();
    			    JTable table = new JTable(model);
    			    
    			  //Adding columns to the JTable
    			    model.addColumn("Heart Rate");
    			    
    			    if(isSpeed){
    			    	model.addColumn("Speed");
    			    }
    			    
    			    if(isCadence){
    			    	model.addColumn("Cadence");
    			    }
    			    
    			    if(isAltitude){
    			    	model.addColumn("Altitude");
    			    }
    			    
    			    if(isPower){
    			    	model.addColumn("Power");
    			    }
    			 
    			 // we reached the section with numbers
    			 while ((line1 = br.readLine()) != null) {
    			    // use String.split to split the line, then convert 
    			    parts = line1.split("\\s+");
    				 
    			    //add the components in their respective arraylists
    			    
    			    if(!isSpeed && !isCadence && !isAltitude && !isPower){
    			    	heartRate.add(Integer.parseInt(parts[0]));
    			    	model.addRow(new Object[] { (Integer.parseInt(parts[0]))+units.get("heartRate") });
    			    }
    			    
    			    //for Speed
    			    else if(isSpeed && !isCadence && !isAltitude && !isPower){
    			    	heartRate.add(Integer.parseInt(parts[0]));
	    				speed.add(Double.parseDouble(parts[1])/10.0);    
	    				model.addRow(new Object[] { (Integer.parseInt(parts[0]))+units.get("heartRate"), Double.parseDouble(parts[1])/10.0+units.get("speed") });
    			    }
    			    else if(isSpeed && isCadence && !isAltitude && !isPower){
    			    	heartRate.add(Integer.parseInt(parts[0]));
	    				speed.add(Double.parseDouble(parts[1])/10.0);
	    				cadence.add(Integer.parseInt(parts[2]));
	    				model.addRow(new Object[] { (Integer.parseInt(parts[0]))+units.get("heartRate"), Double.parseDouble(parts[1])/10.0+units.get("speed"), Integer.parseInt(parts[2])+units.get("cadence") });
    			    }
    			    else if(isSpeed && !isCadence && isAltitude && !isPower){
    			    	heartRate.add(Integer.parseInt(parts[0]));
	    				speed.add(Double.parseDouble(parts[1])/10.0);
	    				altitude.add(Integer.parseInt(parts[2]));
	    				model.addRow(new Object[] { (Integer.parseInt(parts[0]))+units.get("heartRate"), Double.parseDouble(parts[1])/10.0+units.get("speed"), Integer.parseInt(parts[2])+units.get("altitude") });
    			    }
    			    else if(isSpeed && !isCadence && !isAltitude && isPower){
    			    	heartRate.add(Integer.parseInt(parts[0]));
	    				speed.add(Double.parseDouble(parts[1])/10.0);
	    				power.add(Integer.parseInt(parts[2]));
	    				model.addRow(new Object[] { (Integer.parseInt(parts[0]))+units.get("heartRate"), Double.parseDouble(parts[1])/10.0+units.get("speed"), Integer.parseInt(parts[2])+units.get("power") });
    			    }
    			    else if(isSpeed && !isCadence && isAltitude && isPower){
    			    	heartRate.add(Integer.parseInt(parts[0]));
	    				speed.add(Double.parseDouble(parts[1])/10.0);
	    				altitude.add(Integer.parseInt(parts[2]));
	    				power.add(Integer.parseInt(parts[3]));
	    				model.addRow(new Object[] { (Integer.parseInt(parts[0]))+units.get("heartRate"), Double.parseDouble(parts[1])/10.0+units.get("speed"), Integer.parseInt(parts[2])+units.get("altitude"), Integer.parseInt(parts[3])+units.get("power") });
    			    }
    			    else if(isSpeed && isCadence && !isAltitude && isPower){
    			    	heartRate.add(Integer.parseInt(parts[0]));
	    				speed.add(Double.parseDouble(parts[1])/10.0);
	    				cadence.add(Integer.parseInt(parts[2]));
	    				power.add(Integer.parseInt(parts[3]));
	    				model.addRow(new Object[] { (Integer.parseInt(parts[0]))+units.get("heartRate"), Double.parseDouble(parts[1])/10.0+units.get("speed"), Integer.parseInt(parts[2])+units.get("cadence"), Integer.parseInt(parts[3])+units.get("power") });
    			    }
    			    else if(isSpeed && isCadence && isAltitude && !isPower){
    			    	heartRate.add(Integer.parseInt(parts[0]));
	    				speed.add(Double.parseDouble(parts[1])/10.0);
	    				cadence.add(Integer.parseInt(parts[2]));
	    				altitude.add(Integer.parseInt(parts[3]));
	    				model.addRow(new Object[] { (Integer.parseInt(parts[0]))+units.get("heartRate"), Double.parseDouble(parts[1])/10.0+units.get("speed"), Integer.parseInt(parts[2])+units.get("cadence"), Integer.parseInt(parts[3])+units.get("altitude") });
    			    }
    			    
    			    //for Cadence
    			    else if(!isSpeed && isCadence && !isAltitude && !isPower){
    			    	heartRate.add(Integer.parseInt(parts[0]));
	    				cadence.add(Integer.parseInt(parts[1]));    
	    				model.addRow(new Object[] { (Integer.parseInt(parts[0]))+units.get("heartRate"), Integer.parseInt(parts[1])+units.get("cadence") });
    			    }
    			    else if(!isSpeed && isCadence && isAltitude && !isPower){
    			    	heartRate.add(Integer.parseInt(parts[0]));
	    				cadence.add(Integer.parseInt(parts[1]));
	    				altitude.add(Integer.parseInt(parts[2]));
	    				model.addRow(new Object[] { (Integer.parseInt(parts[0]))+units.get("heartRate"), Integer.parseInt(parts[1])+units.get("cadence"), Integer.parseInt(parts[2])+units.get("altitude") });
    			    }
    			    else if(!isSpeed && isCadence && !isAltitude && isPower){
    			    	heartRate.add(Integer.parseInt(parts[0]));
	    				cadence.add(Integer.parseInt(parts[1]));
	    				power.add(Integer.parseInt(parts[2]));
	    				model.addRow(new Object[] { (Integer.parseInt(parts[0]))+units.get("heartRate"), Integer.parseInt(parts[1])+units.get("cadence"), Integer.parseInt(parts[2])+units.get("power") });
    			    }
    			    else if(!isSpeed && isCadence && isAltitude && isPower){
    			    	heartRate.add(Integer.parseInt(parts[0]));
    			    	cadence.add(Integer.parseInt(parts[1]));
	    				altitude.add(Integer.parseInt(parts[2]));
	    				power.add(Integer.parseInt(parts[3]));
	    				model.addRow(new Object[] { (Integer.parseInt(parts[0]))+units.get("heartRate"), cadence.add(Integer.parseInt(parts[1]))+units.get("speed"), Integer.parseInt(parts[2])+units.get("altitude"), Integer.parseInt(parts[3])+units.get("power") });
    			    }
    			    
    			    //for Altitude
    			    else if(!isSpeed && !isCadence && isAltitude && !isPower){
    			    	heartRate.add(Integer.parseInt(parts[0]));
	    				altitude.add(Integer.parseInt(parts[1]));    
	    				model.addRow(new Object[] { (Integer.parseInt(parts[0]))+units.get("heartRate"), Integer.parseInt(parts[1])+units.get("altitude") });
    			    }
    			    else if(!isSpeed && !isCadence && isAltitude && isPower){
    			    	heartRate.add(Integer.parseInt(parts[0]));
	    				altitude.add(Integer.parseInt(parts[1]));
	    				power.add(Integer.parseInt(parts[2]));
	    				model.addRow(new Object[] { (Integer.parseInt(parts[0]))+units.get("heartRate"), Integer.parseInt(parts[1])+units.get("altitude"), Integer.parseInt(parts[2])+units.get("power") });
    			    }
    			    
    			  //for Power
    			    else if(!isSpeed && !isCadence && !isAltitude && isPower){
    			    	heartRate.add(Integer.parseInt(parts[0]));
	    				power.add(Integer.parseInt(parts[1]));    
	    				model.addRow(new Object[] { (Integer.parseInt(parts[0]))+units.get("heartRate"), Integer.parseInt(parts[1])+units.get("power") });
    			    }
    			    
    			    //General
    			    else if(isSpeed && isCadence && isAltitude && isPower){
	   				 	heartRate.add(Integer.parseInt(parts[0]));
	    				speed.add(Double.parseDouble(parts[1])/10.0);
	    				cadence.add(Integer.parseInt(parts[2]));
	    				altitude.add(Integer.parseInt(parts[3]));
	    				power.add(Integer.parseInt(parts[4]));

	    				//display the Data in JTable
	    				model.addRow(new Object[] { (Integer.parseInt(parts[0]))+units.get("heartRate"), Double.parseDouble(parts[1])/10.0+units.get("speed"), Integer.parseInt(parts[2])+units.get("cadence"), Integer.parseInt(parts[3])+units.get("altitude"), Integer.parseInt(parts[4])+units.get("power") });
    			    }
    			    
    			 }
    			 
    			//Add the Summary data to Jpanel fot Display in the JFrame
 			 	JPanel panel = new JPanel(new GridBagLayout());
 			 	 GridBagConstraints c = new GridBagConstraints();
  		        
  		        c.insets = new Insets(10,10,10,10);
  		        
  		        JLabel averageHeartRate = new JLabel("Average Heart Rate: "+averageHeartRate(heartRate)+units.get("heartRate"));
		        JLabel maxHeartRate = new JLabel("Maximum Heart Rate: "+maxHeartRate(heartRate)+units.get("heartRate"));
		        JLabel minHeartRate = new JLabel("Minimum Heart Rate: "+minHeartRate(heartRate)+units.get("heartRate"));
		        
		        /**
		        averageHeartRateMap.put(dates.get(dates.size()-1), averageHeartRate(heartRate));
		        maxHeartRateMap.put(workoutDate, maxHeartRate(heartRate));
		        minHeartRateMap.put(workoutDate, minHeartRate(heartRate));
		        */
 		        
 			 	if(isSpeed){
	 		        JLabel averageSpeed = new JLabel("Average Speed: "+averageSpeed(speed)+units.get("speed"));
	 		        JLabel maxSpeed = new JLabel("Maximum Speed: "+maxSpeed(speed)+units.get("speed"));
	 		        
	 		        averageSpeedMap.put(workoutDate, averageSpeed(speed));
	 		        maxSpeedMap.put(workoutDate, maxSpeed(speed));
	 		        
	 		       c.gridx = 0;
	 		        c.gridy = 1;
	 		        panel.add(averageSpeed,c);
	 		        
	 		        c.gridx = 0;
	 		        c.gridy = 2;
	 		        panel.add(maxSpeed,c);
 			 	}
 			 	
 		        if(isPower){
	 		        JLabel averagePower = new JLabel("Average Power: "+averagePower(power)+units.get("power"));
	 		        JLabel maxPower = new JLabel("Maximum Power: "+maxPower(power)+units.get("power"));
	 		        
	 		       averagePowerMap.put(workoutDate, averagePower(power));
	 		       maxPowerMap.put(workoutDate, maxPower(power));
	 		        
	 		       c.gridx = 0;
	 		        c.gridy = 6;
	 		        panel.add(averagePower,c);
	 		        
	 		        c.gridx = 0;
	 		        c.gridy = 7;
	 		        panel.add(maxPower,c);
 		        }
 		        if(isAltitude){
	 		        JLabel averageAltitude = new JLabel("Average Altitude: "+averageAltitude(altitude)+units.get("altitude"));
	 		        JLabel maxAltitude = new JLabel("Maximum Altitude: "+maxAltitude(altitude)+units.get("altitude"));
	 		        
	 		        averageAltitudeMap.put(workoutDate, averageAltitude(altitude));
	 		        maxAltitudeMap.put(workoutDate, maxAltitude(altitude));
	 		        
	 		       c.gridx = 0;
	 		        c.gridy = 8;
	 		        panel.add(averageAltitude,c);
	 		        
	 		        c.gridx = 0;
	 		        c.gridy = 9;
	 		        panel.add(maxAltitude,c);
 		        }
 		        
 		        
 		        c.gridx = 0;
 		        c.gridy = 3;
 		        panel.add(averageHeartRate,c);
 		        c.gridx = 0;
 		        c.gridy = 4;
 		        panel.add(maxHeartRate,c);
 		        c.gridx = 0;
 		        c.gridy = 5;
 		        panel.add(minHeartRate,c);
 		        
 			 //finally display both the table and summary data in the JFrame 
 		        JFrame f = new JFrame("Data for: "+threadName+" on "+workoutDate);
 			    f.setSize(300, 300);
 			    f.setLayout(new FlowLayout());
 			    f.add(new JScrollPane(panel), BorderLayout.SOUTH);
 			    f.add(new JScrollPane(table), BorderLayout.CENTER);
 			    f.setDefaultCloseOperation(EXIT_ON_CLOSE);
 		        f.setLocationRelativeTo(null);
 		        f.pack();
 			    f.setVisible(true);
 	
 		br.close();
 		// out.close();
 		// out1.close();
 		
        Thread.sleep(50);
      }catch (InterruptedException e) {
         System.out.println("Thread " +  threadName + " interrupted.");
      }catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}System.out.println("Thread " +  threadName + " exiting."); //System.out.println("Date Counts at this point is: "+datesListCount()+" \n MaxSpeedMap Count at this point is: "+maxSpeedMap.size()+" \n averageSpeedMap Count at this point is: "+averageSpeedMap.size()+" \n MaxAltitudeMap Count at this point is: "+maxAltitudeMap.size()+" \n averageAltitudeMap Count at this point is: "+averageAltitudeMap.size()+" \n MaxPowerMap Count at this point is: "+maxPowerMap.size()+" \n averagePowerMap Count at this point is: "+averageHeartRateMap.size());
   //System.out.println("HASH MAP DUMP: " + averageSpeedMap.toString());
   }
   
   public void start () {
      System.out.println("Starting Thread for file: " +  threadName );
      if (t == null) {
         t = new Thread (this, threadName);
         t.start ();
      }
   }
   
	//Methods to calculate the Summary Data
	public double totalDistance(ArrayList<Integer> data) {
		
		double sum = 0;
		for(Integer d : data)
		    sum += d;
		return sum;
	}
	
	public static double averageSpeed(ArrayList<Double> data){
		Double sum = 0.0;
		  if(!data.isEmpty()) {
		    for (Double speed : data) {
		        sum += speed;
		    }
		    return sum.doubleValue() / data.size();
		  }
		  return sum;
		
	}
	
	public static double maxSpeed(ArrayList<Double> speed) {
		
		return Collections.max(speed);
	}
	
	public static double averageHeartRate(ArrayList<Integer> data) {
		
		Integer sum = 0;
		  if(!data.isEmpty()) {
		    for (Integer heartRate : data) {
		        sum += heartRate;
		    }
		    return sum.doubleValue() / data.size();
		  }
		  return sum;
	}
	
	public static double maxHeartRate(ArrayList<Integer> data) {
		
		return Collections.max(data);
	}
	
	public static double minHeartRate(ArrayList<Integer> data) {
		
		return Collections.min(data);
	}
	
	public static double averagePower(ArrayList<Integer> data) {
		
		Integer sum = 0;
		  if(!data.isEmpty()) {
		    for (Integer power : data) {
		        sum += power;
		    }
		    return sum.doubleValue() / data.size();
		  }
		  return sum;
	}
	
	public static double maxPower(ArrayList<Integer> data) {
		
		return Collections.max(data);
	}
	
	public static double averageAltitude(ArrayList<Integer> data) {
		
		Integer sum = 0;
		  if(!data.isEmpty()) {
		    for (Integer altitude : data) {
		        sum += altitude;
		    }
		    return sum.doubleValue() / data.size();
		  }
		  return sum;
	}
	
	public static double maxAltitude(ArrayList<Integer> data) {
		
		return Collections.max(data);
	}
	
	public String datesListCount() {
		return Arrays.toString(dates.toArray());
	}
}

public class Test1 {
	
	 static ArrayList<String> fileNames = new ArrayList<String>();

   public static void main(String args[]) {
	   
	   final File folder = new File("F:/Year 3/Sem C/ASEB/ASDBdata-20160805T031015Z/ASDBdata/August2012DataForCalandar");
		listFilesForFolder(folder);
		//System.out.println(fileNames.size());
		
		int size = fileNames.size();
		
		for(int i=0; i<size; i++){
			RunnableDemo2 R = new RunnableDemo2(fileNames.get(i));
		      R.start();
		     System.out.println(R.datesListCount());
		}
   }
   
   public static void listFilesForFolder(final File folder) {
	    for (final File fileEntry : folder.listFiles()) {
	        if (fileEntry.isDirectory()) {
	            listFilesForFolder(fileEntry);
	        } else {
	            //System.out.println(fileEntry.getName());
	            fileNames.add(fileEntry.getName());
	        }
	    }
	}
}