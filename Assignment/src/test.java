/**
 * 
 */

/**
 * @author Feyi Oshodi
 *
 */

import java.io.*;

public class test {

	/**
	 * @param args
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		FileReader in = null;
	    FileWriter out = null;

	      try {
	         in = new FileReader("test.hrm");
	         out = new FileWriter("output.txt");
	         
	         int c;
	         while ((c = in.read()) != -1) {
	            out.write(c);
	         }
	      } catch(FileNotFoundException e) {
	    	  System.out.print("input file does not exist");
	      }
	      finally {
	         if (in != null) {
	            in.close();
	         }
	         if (out != null) {
	            out.close();
	         }
	         
	         System.out.print("Operation Completed");
	      }
	}

}
