import static javax.swing.JFrame.EXIT_ON_CLOSE; 
import java.awt.*; 
import java.awt.event.*;
import java.awt.print.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.time.Millisecond;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;
/**
 * 
 */

/**
 * @author Feyi Oshodi
 *
 */
public class test2 extends ApplicationFrame implements ActionListener {
	
	public static double averageSpeedValue;
	public static double maxSpeedValue;
	public static double averageAltitudeValue;
	public static double maxAltitudeValue;
	public static double minHeartRateValue;
	public static double maxHeartRateValue;
	public static double averageHeartRateValue;
	public static double averagePowerValue;
	public static double maxPowerValue;
	public boolean isSpeed = true, isCadence = true, isPower = true, isAltitude = true;
	public JPanel content = new JPanel(new BorderLayout());
	
	final Checkbox speedChk = new Checkbox("Toggle Speed");
	final Checkbox powerChk = new Checkbox("Toggle Power");
	final Checkbox heartRateChk = new Checkbox("Toggle Heart Rate");
	final Checkbox altitudeChk = new Checkbox("Toggle Altitude");
	
	private XYPlot plot;
	private JFrame mainFrame;
	private JLabel headerLabel;
	private JLabel statusLabel;
	private JPanel controlPanel;
	private String choice = "not changed";
	private String unit;
	protected boolean isHeartRate;
    private static String fileNameWithPath;

	public test2(String applicationTitle) {
		super( applicationTitle );
		
		prepareGUI(); 
	}
	/**
	 * @param args
	 * @throws IOException 
	 */
		
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		

		test2 chart = new test2("HRData Graph");
		chart.showFileChooserDemo();
		//System.out.println("File NAme: "+chart.showFileChooserDemo());
		//chart.drawGraph("Graphical Representation of the HRData");
	    chart.pack( );        
	    RefineryUtilities.centerFrameOnScreen( chart );        
	    chart.setVisible( true );

	}
	
	private void prepareGUI(){
	      mainFrame = new JFrame("Assignment Part 1");
	      mainFrame.setSize(400,400);
	      mainFrame.setLayout(new GridLayout(3, 1));
	      mainFrame.addWindowListener(new WindowAdapter() {
	         public void windowClosing(WindowEvent windowEvent){
	            System.exit(0);
	         }        
	      });    
	      headerLabel = new JLabel("", JLabel.CENTER);        
	      statusLabel = new JLabel("",JLabel.CENTER);    

	      statusLabel.setSize(350,100);

	      controlPanel = new JPanel();
	      controlPanel.setLayout(new FlowLayout());

	      mainFrame.add(headerLabel);
	      mainFrame.add(controlPanel);
	      mainFrame.add(statusLabel);
	      mainFrame.setVisible(true);  
	   }

	   private String showFileChooserDemo(){
		   //prepareGUI();
		   
	      headerLabel.setText("Please choose a file you want to read from"); 

	      final JFileChooser  fileDialog = new JFileChooser();
	      JButton showFileDialogButton = new JButton("Open File");
	      showFileDialogButton.addActionListener(new ActionListener() {
	         @Override
	         public void actionPerformed(ActionEvent e) {
	            int returnVal = fileDialog.showOpenDialog(mainFrame);
	            if (returnVal == JFileChooser.APPROVE_OPTION) {
	               java.io.File file = fileDialog.getSelectedFile();
	               statusLabel.setText("File Selected :" 
	               + file.getName());
	               fileNameWithPath = file.toString();
	               
	               try {
					doSomething(fileNameWithPath);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
	             /** for Debugging purposes only
	               System.out.println(file.toString());
	               System.out.println("File Selected :" 
	               + file.getName());
	               */
	            }
	            else{
	               statusLabel.setText("Open command cancelled by user." ); 
	            }      
	         }
	      });
	      controlPanel.add(showFileDialogButton);
	      mainFrame.setVisible(true);  
	      
	      return fileNameWithPath;
	   }
	
	private void doSomething(String fileName) throws IOException {
		//Declare and initialize all needed variables
	    String [] parts;
	    int col = 0;
		 String line, line1;
		 int hour, minute, second, totalTimeTaken;
		String startTime, timeOfDay = "am";
		 boolean isSpeed = true, isCadence = true, isPower = true, isAltitude = true; isHeartRate = true;
	    
	    //create a buffered reader to read from and write to file
		 BufferedReader br = new BufferedReader(new FileReader(fileName));
		 BufferedWriter out1 = new BufferedWriter(new FileWriter("output1.txt"));
		 BufferedWriter out = new BufferedWriter(new FileWriter("output.txt"));
		 
		 //create arraylist for all HRData for easy management
//		 ArrayList<ArrayList<Integer>> speed = new ArrayList<ArrayList<Integer>>();
		 ArrayList<Double> speed = new ArrayList<Double>();
		 ArrayList<Integer> cadence = new ArrayList<Integer>();
		 ArrayList<Integer> altitude = new ArrayList<Integer>();
		 ArrayList<Integer> heartRate = new ArrayList<Integer>();
		 ArrayList<Integer> power = new ArrayList<Integer>();
		 Map<String, String> params = new HashMap<String, String>();
		 Map<String, String> units = new HashMap<String, String>();
		 
		 //begin reading from the file and capture data after the word "Monitor" is found
		 while ((line = br.readLine()) != null) {
		  if (line.contains("Monitor")){
		    // do something
		    break; // breaks the while loop
		  }
		 }
		 
		 // we reached the section we want
		 while ((line = br.readLine()) != null) {
		    
			 // read everything before the word "Interval"
			 if (line.contains("Interval")){
				    // do something
				    break; // breaks the while loop
				  }
			 
			 //split the [Params] into their components and put them in the Map variable params
			 parts = line.split("\\=");
			
			params.put(parts[0], parts[1]);
			
			System.out.println(parts[0] +" - "+ params.get(parts[0]));
			
			col +=1;
			
			//for debugging purposes
			//System.out.print(Arrays.toString(parts));
			 
			//Write the captured data into a file.
			 out1.write(line);
			 out1.write(params.get(parts[0]));
			 //out.write(String.format("%10s%10s%10s%10s%10s%10s",parts[0],parts[1],parts[2],parts[3],parts[4], parts[5]));
				out1.newLine();
		 }
		 
		 String[] timeStarted = params.get("StartTime").split("(?<=.)");
		 hour = Integer.parseInt(timeStarted[0]+timeStarted[1]);
		 minute = Integer.parseInt(timeStarted[3]+timeStarted[4]);
		 second = Integer.parseInt(timeStarted[6]+timeStarted[7]);
		 if(hour >=12) {
			 if((hour-12) > 0){
				 hour = hour-12;
			 }
			 timeOfDay ="pm";
		 }
		 startTime = hour + ":" + minute + timeOfDay;
		 System.out.println(startTime);
		 
		 String[] timeTaken = params.get("Length").split("(?<=.)");
		 //System.out.print(Arrays.toString(totalTimeTaken));
		 hour = Integer.parseInt(timeTaken[0]+timeTaken[1]);
		 minute = Integer.parseInt(timeTaken[3]+timeTaken[4]);
		 second = Integer.parseInt(timeTaken[6]+timeTaken[7]);
		 totalTimeTaken = (hour*60) + minute;
		 if(second >=50) {
			 totalTimeTaken++;
		 }
		 System.out.println(totalTimeTaken);
		 
		 String[] dateFromFile = params.get("Date").split("(?<=.)");
		 String workoutDate = dateFromFile[6]+dateFromFile[7]+"/"+dateFromFile[4]+dateFromFile[5]+"/"+dateFromFile[0]+dateFromFile[1]+dateFromFile[2]+dateFromFile[3];
		 
		 
		 // Get the last last bit of the SMode to determine the units of the measurements
		 //char[] digits = params.get("SMode").toCharArray();
		 
		 String[] digits = params.get("SMode").split("(?<=.)");
		 
		// System.out.print(digits[digits.length-1]);
		 
		 if(digits[0].equalsIgnoreCase("0")){
			 isSpeed = false;
			 
			 System.out.println("Speed is not recorded!!");
		 }
		 
		 if(digits[1].equalsIgnoreCase("0")){
			 isCadence = false;
			 
			 System.out.println("Cadence is not recorded!!");
		 }
		 
		 if(digits[2].equalsIgnoreCase("0")){
			 isAltitude = false;
			 
			 System.out.println("Altitude is not recorded!!");
		 }
		 
		 if(digits[3].equalsIgnoreCase("0")){
			 isPower = false;
			 
			 System.out.println("Power is not recorded!!");
		 }
		 
		 if(digits.length == 9) {
			unit = digits[(digits.length -1)];
			 
			 //System.out.print("Smode is 9, so Unit is: "+unit);
		 } else if(digits.length == 8) {
			 unit = digits[(digits.length)];
		 }		 
		 	
		 //if the bit = 0, then the unit is metric
		 if(unit.equalsIgnoreCase("0")) {
			 
			//JDialog.setDefaultLookAndFeelDecorated(true);
			    int response = JOptionPane.showConfirmDialog(null, "Unit is already in Metric(Kilometers, meters, celcius) \n Do you want to continue? \n press yes to continue and No to change to Imperial units", "Confirm",
			        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			    if (response == JOptionPane.NO_OPTION) {
			      //System.out.println("No button clicked");
			    	//unit = "1";
			    	response = JOptionPane.showConfirmDialog(null, "Unit changed to Imperial(Miles, Feets, Farenheit) \n Do you want to continue? \n press yes to continue and No to change to back to Metric units", "Confirm",
					        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
					    if (response == JOptionPane.NO_OPTION) {
					      //System.out.println("No button clicked");
					    	JOptionPane.showMessageDialog(null, "Units changed back to Metic(Kilometers, meters, celcius)");
					    	unit = "0";
					    } else if (response == JOptionPane.YES_OPTION) {
					      //System.out.println("Yes button clicked");
					    	unit = "1";
					    	choice = "changed";
					    	JOptionPane.showMessageDialog(null, "Units will be displayed in Imperial(Miles, Feets, Farenheit)");
					    } else if (response == JOptionPane.CLOSED_OPTION) {
					      //System.out.println("JOptionPane closed");
					    	JOptionPane.showMessageDialog(null, "Operation Cancelled by User, please start all over");
					    }
			    } else if (response == JOptionPane.YES_OPTION) {
			      //System.out.println("Yes button clicked");
			    	JOptionPane.showMessageDialog(null, "Units will be displayed in Metric(Kilometers, meters, celcius)");
			    } else if (response == JOptionPane.CLOSED_OPTION) {
			      //System.out.println("JOptionPane closed");
			    	JOptionPane.showMessageDialog(null, "Operation Cancelled by User, please start all over");
			    }
			 
			 /** For Debugging purposes
			 System.out.print("Unit is 0");
			 System.out.print("Speed is: " + units.get("speed"));
			 */
		 } 
		 // if the bit is 1, then the unit is imperial
		 else if(unit.equalsIgnoreCase("1")) {
			 
			 int response = JOptionPane.showConfirmDialog(null, "Unit is already in Imperial(Miles, Feets, Farenheit) \n Do you want to continue? \n press yes to continue and No to change to Metric units", "Confirm",
			 JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			 if (response == JOptionPane.NO_OPTION) {
				 //System.out.println("No button clicked");
				 //unit = "1";
				 response = JOptionPane.showConfirmDialog(null, "Unit changed to Metric(Kilometers, meters, celcius) \n Do you want to continue? \n press yes to continue and No to change to back to Imperial units", "Confirm",
						 JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				 if (response == JOptionPane.NO_OPTION) {
					 //System.out.println("No button clicked");
					 JOptionPane.showMessageDialog(null, "Units changed back to Imperial(Miles, Feets, Farenheit)");
					 unit = "1";
				 } else if (response == JOptionPane.YES_OPTION) {
					 //System.out.println("Yes button clicked");
					 unit = "0";
					 choice = "changed";
					 JOptionPane.showMessageDialog(null, "Units will be displayed in Metric(Kilometers, meters, celcius)");
				 } else if (response == JOptionPane.CLOSED_OPTION) {
					 //System.out.println("JOptionPane closed");
					 JOptionPane.showMessageDialog(null, "Operation Cancelled by User, please start all over");
				 }
			 } else if (response == JOptionPane.YES_OPTION) {
				 //System.out.println("Yes button clicked");
				 JOptionPane.showMessageDialog(null, "Units will be displayed in Imperial(Miles, Feets, Farenheit)");
			 } else if (response == JOptionPane.CLOSED_OPTION) {
				 //System.out.println("JOptionPane closed");
				 JOptionPane.showMessageDialog(null, "Operation Cancelled by User, please start all over");
			 }
			 
			 //System.out.print("Unit is 1");
		 }		
		 
		 if(unit.equalsIgnoreCase("1")){
			 units.put("speed", "mph");
			 units.put("dist", "m");
			 units.put("altitude", "ft");
			 units.put("temp", "oF");
			 units.put("power", "watts");
			 units.put("cadence", "rpm");
			 units.put("heartRate", "bpm");
		 } else if(unit.equalsIgnoreCase("0")){
			 units.put("speed", "km/h");
			 units.put("dist", "km");
			 units.put("altitude", "m");
			 units.put("temp", "oC");
			 units.put("power", "watts");
			 units.put("cadence", "rpm");
			 units.put("heartRate", "bpm");
		 }
		 
		// System.out.print(params.get(0).get(0).toString());
		 
		 //proceed to reda in the [HRData]
		 while ((line1 = br.readLine()) != null) {
			  if (line1.contains("[HRData]")){
			    // do something
			    break; // breaks the while loop
			  }
			 }

		 //print the HRData out in table format
			 out.write(String.format("%10s%10s%10s%10s%10s%10s","Speed","| Cadence","| Altitude","| Heart Rate","| Power","| other"));
				out.newLine();
				out.write("-------------------------------------------------------------------------------");
				out.newLine();
				
				//print the HRData out in JTable
				DefaultTableModel model = new DefaultTableModel();
			    JTable table = new JTable(model);
			    
			    //Adding columns to the JTable
			    model.addColumn("Heart Rate");
			    
			    if(isSpeed){
			    	model.addColumn("Speed");
			    	speedChk.setState(true);
			    }
			    
			    if(isCadence){
			    	model.addColumn("Cadence");
			    }
			    
			    if(isAltitude){
			    	model.addColumn("Altitude");
			    	altitudeChk.setState(true);
			    }
			    
			    if(isPower){
			    	model.addColumn("Power");
			    	powerChk.setState(true);
			    }
			    heartRateChk.setState(true);
			 
			 // we reached the section with numbers
			 while ((line1 = br.readLine()) != null) {
			    // use String.split to split the line, then convert 
			    parts = line1.split("\\s+");
			    
			  //add the components in their respective arraylists
			    
			    if(!isSpeed && !isCadence && !isAltitude && !isPower){
			    	heartRate.add(Integer.parseInt(parts[0]));
			    	model.addRow(new Object[] { (Integer.parseInt(parts[0]))+units.get("heartRate") });
			    }
			    
			    //for Speed
			    else if(isSpeed && !isCadence && !isAltitude && !isPower){
			    	heartRate.add(Integer.parseInt(parts[0]));
    				speed.add(Double.parseDouble(parts[1])/10.0);    
    				model.addRow(new Object[] { (Integer.parseInt(parts[0]))+units.get("heartRate"), Double.parseDouble(parts[1])/10.0+units.get("speed") });
			    }
			    else if(isSpeed && isCadence && !isAltitude && !isPower){
			    	heartRate.add(Integer.parseInt(parts[0]));
    				speed.add(Double.parseDouble(parts[1])/10.0);
    				cadence.add(Integer.parseInt(parts[2]));
    				model.addRow(new Object[] { (Integer.parseInt(parts[0]))+units.get("heartRate"), Double.parseDouble(parts[1])/10.0+units.get("speed"), Integer.parseInt(parts[2])+units.get("cadence") });
			    }
			    else if(isSpeed && !isCadence && isAltitude && !isPower){
			    	heartRate.add(Integer.parseInt(parts[0]));
    				speed.add(Double.parseDouble(parts[1])/10.0);
    				altitude.add(Integer.parseInt(parts[2]));
    				model.addRow(new Object[] { (Integer.parseInt(parts[0]))+units.get("heartRate"), Double.parseDouble(parts[1])/10.0+units.get("speed"), Integer.parseInt(parts[2])+units.get("altitude") });
			    }
			    else if(isSpeed && !isCadence && !isAltitude && isPower){
			    	heartRate.add(Integer.parseInt(parts[0]));
    				speed.add(Double.parseDouble(parts[1])/10.0);
    				power.add(Integer.parseInt(parts[2]));
    				model.addRow(new Object[] { (Integer.parseInt(parts[0]))+units.get("heartRate"), Double.parseDouble(parts[1])/10.0+units.get("speed"), Integer.parseInt(parts[2])+units.get("power") });
			    }
			    else if(isSpeed && !isCadence && isAltitude && isPower){
			    	heartRate.add(Integer.parseInt(parts[0]));
    				speed.add(Double.parseDouble(parts[1])/10.0);
    				altitude.add(Integer.parseInt(parts[2]));
    				power.add(Integer.parseInt(parts[3]));
    				model.addRow(new Object[] { (Integer.parseInt(parts[0]))+units.get("heartRate"), Double.parseDouble(parts[1])/10.0+units.get("speed"), Integer.parseInt(parts[2])+units.get("altitude"), Integer.parseInt(parts[3])+units.get("power") });
			    }
			    else if(isSpeed && isCadence && !isAltitude && isPower){
			    	heartRate.add(Integer.parseInt(parts[0]));
    				speed.add(Double.parseDouble(parts[1])/10.0);
    				cadence.add(Integer.parseInt(parts[2]));
    				power.add(Integer.parseInt(parts[3]));
    				model.addRow(new Object[] { (Integer.parseInt(parts[0]))+units.get("heartRate"), Double.parseDouble(parts[1])/10.0+units.get("speed"), Integer.parseInt(parts[2])+units.get("cadence"), Integer.parseInt(parts[3])+units.get("power") });
			    }
			    else if(isSpeed && isCadence && isAltitude && !isPower){
			    	heartRate.add(Integer.parseInt(parts[0]));
    				speed.add(Double.parseDouble(parts[1])/10.0);
    				cadence.add(Integer.parseInt(parts[2]));
    				altitude.add(Integer.parseInt(parts[3]));
    				model.addRow(new Object[] { (Integer.parseInt(parts[0]))+units.get("heartRate"), Double.parseDouble(parts[1])/10.0+units.get("speed"), Integer.parseInt(parts[2])+units.get("cadence"), Integer.parseInt(parts[3])+units.get("altitude") });
			    }
			    
			    //for Cadence
			    else if(!isSpeed && isCadence && !isAltitude && !isPower){
			    	heartRate.add(Integer.parseInt(parts[0]));
    				cadence.add(Integer.parseInt(parts[1]));    
    				model.addRow(new Object[] { (Integer.parseInt(parts[0]))+units.get("heartRate"), Integer.parseInt(parts[1])+units.get("cadence") });
			    }
			    else if(!isSpeed && isCadence && isAltitude && !isPower){
			    	heartRate.add(Integer.parseInt(parts[0]));
    				cadence.add(Integer.parseInt(parts[1]));
    				altitude.add(Integer.parseInt(parts[2]));
    				model.addRow(new Object[] { (Integer.parseInt(parts[0]))+units.get("heartRate"), Integer.parseInt(parts[1])+units.get("cadence"), Integer.parseInt(parts[2])+units.get("altitude") });
			    }
			    else if(!isSpeed && isCadence && !isAltitude && isPower){
			    	heartRate.add(Integer.parseInt(parts[0]));
    				cadence.add(Integer.parseInt(parts[1]));
    				power.add(Integer.parseInt(parts[2]));
    				model.addRow(new Object[] { (Integer.parseInt(parts[0]))+units.get("heartRate"), Integer.parseInt(parts[1])+units.get("cadence"), Integer.parseInt(parts[2])+units.get("power") });
			    }
			    else if(!isSpeed && isCadence && isAltitude && isPower){
			    	heartRate.add(Integer.parseInt(parts[0]));
			    	cadence.add(Integer.parseInt(parts[1]));
    				altitude.add(Integer.parseInt(parts[2]));
    				power.add(Integer.parseInt(parts[3]));
    				model.addRow(new Object[] { (Integer.parseInt(parts[0]))+units.get("heartRate"), cadence.add(Integer.parseInt(parts[1]))+units.get("speed"), Integer.parseInt(parts[2])+units.get("altitude"), Integer.parseInt(parts[3])+units.get("power") });
			    }
			    
			    //for Altitude
			    else if(!isSpeed && !isCadence && isAltitude && !isPower){
			    	heartRate.add(Integer.parseInt(parts[0]));
    				altitude.add(Integer.parseInt(parts[1]));    
    				model.addRow(new Object[] { (Integer.parseInt(parts[0]))+units.get("heartRate"), Integer.parseInt(parts[1])+units.get("altitude") });
			    }
			    else if(!isSpeed && !isCadence && isAltitude && isPower){
			    	heartRate.add(Integer.parseInt(parts[0]));
    				altitude.add(Integer.parseInt(parts[1]));
    				power.add(Integer.parseInt(parts[2]));
    				model.addRow(new Object[] { (Integer.parseInt(parts[0]))+units.get("heartRate"), Integer.parseInt(parts[1])+units.get("altitude"), Integer.parseInt(parts[2])+units.get("power") });
			    }
			    
			  //for Power
			    else if(!isSpeed && !isCadence && !isAltitude && isPower){
			    	heartRate.add(Integer.parseInt(parts[0]));
    				power.add(Integer.parseInt(parts[1]));    
    				model.addRow(new Object[] { (Integer.parseInt(parts[0]))+units.get("heartRate"), Integer.parseInt(parts[1])+units.get("power") });
			    }
			    
			    //General
			    else if(isSpeed && isCadence && isAltitude && isPower){
   				 	heartRate.add(Integer.parseInt(parts[0]));
    				speed.add(Double.parseDouble(parts[1])/10.0);
    				cadence.add(Integer.parseInt(parts[2]));
    				altitude.add(Integer.parseInt(parts[3]));
    				power.add(Integer.parseInt(parts[4]));

    				//display the Data in JTable
    				model.addRow(new Object[] { (Integer.parseInt(parts[0]))+units.get("heartRate"), Double.parseDouble(parts[1])/10.0+units.get("speed"), Integer.parseInt(parts[2])+units.get("cadence"), Integer.parseInt(parts[3])+units.get("altitude"), Integer.parseInt(parts[4])+units.get("power") });
			    }
				
			    /**
			    //add the components in their respective arraylists
				 speed.add(Double.parseDouble(parts[0])/10.0);
				 cadence.add(Integer.parseInt(parts[1]));
				 altitude.add(Integer.parseInt(parts[2]));
				 heartRate.add(Integer.parseInt(parts[3]));
				 power.add(Integer.parseInt(parts[4]));
				 
				 //display the Data in JTable
				 model.addRow(new Object[] { (Integer.parseInt(parts[0]))+units.get("heartRate"), Integer.parseInt(parts[1])+units.get("speed"), Integer.parseInt(parts[2])+units.get("cadence"), Integer.parseInt(parts[3])+units.get("height"), Integer.parseInt(parts[4])+units.get("power") });				 
				 
//				 out.write(" ");
//				 out.write(String.format("%10s%10s%10s%10s%10s%10s",parts[0],parts[1],parts[2],parts[3],parts[4], parts[5]));
//					out.newLine();				 
				 */
			 }
			 
				
				averageHeartRateValue = averageHeartRate(heartRate, unit, choice);
				maxHeartRateValue = maxHeartRate(heartRate, unit, choice);
				minHeartRateValue = minHeartRate(heartRate, unit, choice);
				
				
			 
			 	//Add the Summary data to Jpanel fot Display in the JFrame
			 	JPanel panel = new JPanel(new GridBagLayout());
			 	GridBagConstraints c = new GridBagConstraints();
  		        
  		        c.insets = new Insets(10,10,10,10);
  		        
  		        JLabel averageHeartRate = new JLabel("Average Heart Rate: "+averageHeartRateValue+units.get("heartRate"));
		        JLabel maxHeartRate = new JLabel("Maximum Heart Rate: "+maxHeartRateValue+units.get("heartRate"));
		        JLabel minHeartRate = new JLabel("Minimum Heart Rate: "+minHeartRateValue+units.get("heartRate"));
		        
		        if(isSpeed){
		        	averageSpeedValue = averageSpeed(speed, unit, choice);
					maxSpeedValue = maxSpeed(speed, unit, choice);
		        	
	 		        JLabel averageSpeed = new JLabel("Average Speed: "+averageSpeedValue+units.get("speed"));
	 		        JLabel maxSpeed = new JLabel("Maximum Speed: "+maxSpeedValue+units.get("speed"));
	 		        
	 		       c.gridx = 0;
	 		        c.gridy = 1;
	 		        panel.add(averageSpeed,c);
	 		        
	 		        c.gridx = 0;
	 		        c.gridy = 2;
	 		        panel.add(maxSpeed,c);
 			 	}
 			 	
 		        if(isPower){
 		        	averagePowerValue = averagePower(power, unit, choice);
 					maxPowerValue = maxPower(power, unit, choice);
 		        	
	 		        JLabel averagePower = new JLabel("Average Power: "+averagePowerValue+units.get("power"));
	 		        JLabel maxPower = new JLabel("Maximum Power: "+maxPowerValue+units.get("power"));

	 		       c.gridx = 0;
	 		        c.gridy = 6;
	 		        panel.add(averagePower,c);
	 		        
	 		        c.gridx = 0;
	 		        c.gridy = 7;
	 		        panel.add(maxPower,c);
 		        }
 		        if(isAltitude){
 		        	averageAltitudeValue = averageAltitude(altitude, unit, choice);
 					maxAltitudeValue = maxAltitude(altitude, unit, choice);
 		        	
	 		        JLabel averageAltitude = new JLabel("Average Altitude: "+averageAltitudeValue+units.get("altitude"));
	 		        JLabel maxAltitude = new JLabel("Maximum Altitude: "+maxAltitudeValue+units.get("altitude"));
	 		        
	 		       c.gridx = 0;
	 		        c.gridy = 8;
	 		        panel.add(averageAltitude,c);
	 		        
	 		        c.gridx = 0;
	 		        c.gridy = 9;
	 		        panel.add(maxAltitude,c);
 		        }
		        
			 //finally display both the table and summary data in the JFrame 
		        JFrame f = new JFrame("Data for the workout session on "+workoutDate+" at "+startTime);
			    f.setSize(300, 300);
			    f.setLayout(new FlowLayout());
			    f.add(new JScrollPane(panel), BorderLayout.SOUTH);
			    f.add(new JScrollPane(table), BorderLayout.CENTER);
			    f.setDefaultCloseOperation(EXIT_ON_CLOSE);
		        f.setLocationRelativeTo(null);
		        f.pack();
			    f.setVisible(true);
			 
			 int size = speed.size();
			 
	//Print out the Data to a text file
	for(int i=0; i<size; i++){
		 out.write(" ");
		 out.write(String.format("%10s%10s%10s%10s%10s%10s",speed.get(i)+units.get("speed"),cadence.get(i)+units.get("cadence"),altitude.get(i)+units.get("height"),heartRate.get(i)+units.get("heartRate"),power.get(i)+units.get("power"), ""));
			out.newLine();
	}
	
	//Print the summary data to the console 
	//out1.write(averageSpeed(speed));
	System.out.println();
	System.out.println("Average Speed: "+averageSpeedValue+units.get("speed"));
	System.out.println("Maximum Speed: "+maxSpeedValue+units.get("speed"));
	System.out.println("Average Heart Rate: "+averageHeartRateValue+units.get("heartRate"));
	System.out.println("Maximum Heart Rate: "+maxHeartRateValue+units.get("heartRate"));
	System.out.println("Minimum Heart Rate: "+minHeartRateValue+units.get("heartRate"));
	System.out.println("Average Power: "+averagePowerValue+units.get("power"));
	System.out.println("Maximum Power: "+maxPowerValue+units.get("power"));
	System.out.println("Average Altitude: "+averageAltitudeValue+units.get("altitude"));
	System.out.println("Maximum Altitude: "+maxAltitudeValue+units.get("altitude"));
	
	drawGraph();
	
		br.close();
		out.close();
		out1.close();
	}
	
	//Methods to calculate the Summary Data
	public double totalDistance(ArrayList<Integer> data, String mode, String Choice ) {
		
		double sum = 0;
		for(Integer d : data)
		    sum += d;
		
		System.out.println("Mode is: "+mode+" and Choice is: "+Choice);
		
		if(mode.equalsIgnoreCase("1") && Choice.equalsIgnoreCase("changed")){
			sum = (sum*1.61);
		} else if(mode.equalsIgnoreCase("0") && Choice.equalsIgnoreCase("changed")){
			sum = (sum*0.62);
		}
		
		return sum;
	}
	
	public static double averageSpeed(ArrayList<Double> data, String mode, String Choice){
		Double sum = 0.0;
		  if(!data.isEmpty()) {
		    for (Double speed : data) {
		        sum += speed;
		    }
		    return sum.doubleValue() / data.size();
		  }
		  
		  System.out.println("Mode is: "+mode+" and Choice is: "+Choice);
		  
		  if(mode.equalsIgnoreCase("1") && Choice.equalsIgnoreCase("changed")){
				sum = (sum*1.61);
			} else if(mode.equalsIgnoreCase("0") && Choice.equalsIgnoreCase("changed")){
				sum = (sum*0.62);
			}
		  
		  return sum;
		
	}
	
	public static double maxSpeed(ArrayList<Double> speed, String mode, String Choice) {
		
		double sum = Collections.max(speed);
		
		System.out.println("Mode is: "+mode+" and Choice is: "+Choice);
		
		if(mode.equalsIgnoreCase("1") && Choice.equalsIgnoreCase("changed")){
			sum = (sum*1.61);
		} else if(mode.equalsIgnoreCase("0") && Choice.equalsIgnoreCase("changed")){
			sum = (sum*0.62);
		}
		
		return sum;
	}
	
	public static double averageHeartRate(ArrayList<Integer> data, String mode, String Choice) {
		
		Integer sum = 0;
		  if(!data.isEmpty()) {
		    for (Integer heartRate : data) {
		        sum += heartRate;
		    }
		    return sum.doubleValue() / data.size();
		  }
		  return sum;
	}
	
	public static double maxHeartRate(ArrayList<Integer> data, String mode, String Choice) {
		
		return Collections.max(data);
	}
	
	public static double minHeartRate(ArrayList<Integer> data, String mode, String Choice) {
		
		return Collections.min(data);
	}
	
	public static double averagePower(ArrayList<Integer> data, String mode, String Choice) {
		
		Integer sum = 0;
		  if(!data.isEmpty()) {
		    for (Integer power : data) {
		        sum += power;
		    }
		    return sum.doubleValue() / data.size();
		  }
		  return sum;
	}
	
	public static double maxPower(ArrayList<Integer> data, String mode, String Choice) {
		
		return Collections.max(data);
	}
	
	public static double averageAltitude(ArrayList<Integer> data, String mode, String Choice) {
		
		Integer sum = 0;
		  if(!data.isEmpty()) {
		    for (Integer altitude : data) {
		        sum += altitude;
		    }
		    return sum.doubleValue() / data.size();
		  }
		  return sum;
	}
	
	public static double maxAltitude(ArrayList<Integer> data, String mode, String Choice) {
		
		return Collections.max(data);
	}
	
	   private CategoryDataset createDataset( )
	   {        
	      final String average = "Average";
	      final String maximum = "Maximum";
	      final String minimum = "Mininimum";
	      final String speedData = "Speed";
	      final String heartRateData = "Heart Rate";
	      final String powerData = "Power";
	      final String altitudeData = "Altitude";
	      final DefaultCategoryDataset dataset = 
	      new DefaultCategoryDataset( );  

//	      dataset.addValue( 1.0 , fiat , speed );        
//	      dataset.addValue( 3.0 , fiat , userrating );        
//	      dataset.addValue( 5.0 , fiat , millage ); 
//	      dataset.addValue( 5.0 , fiat , safety );           
	//
//	      dataset.addValue( 5.0 , audi , speed );        
//	      dataset.addValue( 6.0 , audi , userrating );       
//	      dataset.addValue( 10.0 , audi , millage );        
//	      dataset.addValue( 4.0 , audi , safety );
	      
	      if(isSpeed){
		      dataset.addValue( averageSpeedValue, average, speedData );        
		      dataset.addValue( maxSpeedValue, maximum, speedData );
	      }
	      if(isPower){
		      dataset.addValue( averagePowerValue, average, powerData );        
		      dataset.addValue( maxPowerValue, maximum, powerData );
	      }
	      if(isAltitude){
		      dataset.addValue( averageAltitudeValue, average, altitudeData );
		      dataset.addValue( maxAltitudeValue, maximum, altitudeData );
	      }
	      if(isHeartRate){
		      dataset.addValue( averageHeartRateValue, average, heartRateData );
		      dataset.addValue( maxHeartRateValue, maximum, heartRateData );
		      dataset.addValue( minHeartRateValue, minimum, heartRateData );
	      }

	      return dataset; 
	   }
	   
	   public void drawGraph() {
		   String chartTitle = "Graphical Representation of the HRData";
		   JFreeChart barChart = ChartFactory.createBarChart(
			         chartTitle,           
			         "Statistics",            
			         "Value",            
			         createDataset(),          
			         PlotOrientation.VERTICAL,           
			         true, true, false);
		   
		   ChartPanel chartPanel = new ChartPanel( barChart ) {
			   @Override
	            public Dimension getPreferredSize() {
	                return new Dimension(320, 240);
	            }
		   };  
		   
		   speedChk.addItemListener(new ItemListener() {
		         public void itemStateChanged(ItemEvent e) {             
		            isSpeed =  (e.getStateChange()==1?true:false);
		           // JOptionPane.showMessageDialog(null, "Speed is "+isSpeed+" Now!!");
		            refreshChart();
		         }
		      });
		   
		   powerChk.addItemListener(new ItemListener() {
		         public void itemStateChanged(ItemEvent e) {             
		            isPower =  (e.getStateChange()==1?true:false);
		          //  JOptionPane.showMessageDialog(null, "Power is "+isPower+" Now!!");
		            refreshChart();
		         }
		      });
		  
		   heartRateChk.addItemListener(new ItemListener() {
		         public void itemStateChanged(ItemEvent e) {             
		            isHeartRate =  (e.getStateChange()==1?true:false);
		           // JOptionPane.showMessageDialog(null, "Heart Rate is "+isHeartRate+" Now!!");
		            refreshChart();
		         }
		      });
		   
		   altitudeChk.addItemListener(new ItemListener() {
		         public void itemStateChanged(ItemEvent e) {             
		            isAltitude =  (e.getStateChange()==1?true:false);
		           // JOptionPane.showMessageDialog(null, "Altitude is "+isAltitude+" Now!!");
		            refreshChart();
		         }
		      });
	        
	        content.add(chartPanel);
	        JPanel subPanel = new JPanel();
	        subPanel.add(speedChk);
	        subPanel.add(powerChk);
	        subPanel.add(heartRateChk);
	        subPanel.add(altitudeChk);
	        content.add(subPanel, BorderLayout.SOUTH);
		   chartPanel.setPreferredSize(new java.awt.Dimension( 560 , 367 ) );        
		   setContentPane( content );
	   }
	   
	   public void refreshChart() {
		   content.removeAll();
		   content.setLayout(new BorderLayout());
		   drawGraph();
		   content.validate();
		   content.repaint();
	   }
	@Override
	public void actionPerformed(final ActionEvent e) {
		// TODO Auto-generated method stub
		
		if (e.getActionCommand().equals("REMOVE HEART RATE")) {
            
            System.out.println("Heart Rate Removed");
        }
	}
	
}
